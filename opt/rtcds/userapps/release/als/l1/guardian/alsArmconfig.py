#######
#ALS ARM PARAMETERS FILE
#######
# Tested values good for HWS
#pos_yawoffset4tcs = {'ALS_XARM':0.80,'ALS_YARM':0.60}
#ang_yawoffset4tcs = {'ALS_XARM':0.50,'ALS_YARM':0.50}
#Trial values for working transistion
pos_pitoffset4tcs = {'ALS_XARM':-0.20,'ALS_YARM':-0.40}
ang_pitoffset4tcs = {'ALS_XARM':0.00,'ALS_YARM':0.00}
pos_yawoffset4tcs = {'ALS_XARM':-0.20,'ALS_YARM':0.00}
ang_yawoffset4tcs = {'ALS_XARM':-0.50,'ALS_YARM':0.60}

#pos_pitoffset4tcs = {'ALS_XARM':0.80,'ALS_YARM':0.80}
#ang_pitoffset4tcs = {'ALS_XARM':0.80,'ALS_YARM':0.80}
#pos_yawoffset4tcs = {'ALS_XARM':0.60,'ALS_YARM':0.80}
#ang_yawoffset4tcs = {'ALS_XARM':0.60,'ALS_YARM':0.80}

#Nominal values for normal operation 
#pos_yawoffset4servo = {'ALS_XARM':0.60,'ALS_YARM':-0.30} #AJM changed to match nominal IR alignment 161020
pos_pitoffset4servo = {'ALS_XARM':0.40,'ALS_YARM':0.00}		#XARM was 0.3 200707 #YARM was 0.4 20210301
ang_pitoffset4servo = {'ALS_XARM':0.00,'ALS_YARM':0.00}
pos_yawoffset4servo = {'ALS_XARM':0.40,'ALS_YARM':0.11}	#was 0.2 20220513 #YARM was -0.4 20201201
ang_yawoffset4servo = {'ALS_XARM':0.00,'ALS_YARM':-0.10}		#YARM was -0.3 20210301


#Servo settings
pdh_acq_gain = {'ALS_XARM':-9,'ALS_YARM':1}    #YARM decreased from 5dB to 1dB (AJM211105)
pdh_lock_gain = {'ALS_XARM':-4,'ALS_YARM':2}	#YARM decreased from 9dB to 5dB (AJM211109), decreased again 5dB to 2dB (AJM220616)

#Threshold for locking PDH
transpd_threshold = {'ALS_XARM':450,'ALS_YARM':500}
#transpd_threshold = {'COMM':-1,'DIFF':-27} # TH 180926 changed to monitor diff

#Beat note frequency for PLL
beatnote_set = {'ALS_XARM':-39.6,'ALS_YARM':79.2}


drive_mtrx = {'P':[[-1.07,1.0],
                     [-1.0,0.78]],
                'Y':[[1.07,1.0],
                     [-1.0,-0.78]]
               }

cam_spots_nom = {'ALS_XARM':
                 {'ITM':{'P':38.0,'Y':-75.2},
                  'ETM':{'P':-18.7,'Y':-1.7}},
                 'ALS_YARM':
                 {'ITM':{'P':-20.5,'Y':-47.0},
                  'ETM':{'P':-92.8,'Y':133.1}}
                }
