# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-

import time, alsArmconfig
import cdsutils
from guardian import GuardState, GuardStateDecorator
from laserFreqScan import LaserFreqScan
import numpy as np

import runningAvg

# TODO: Decorator for PLL
# TODO: Decorator for Laser
# TODO: Check to see that QPDs are actually locked at the correct position

nominal = 'SHUTTERED'

##################################################

def shutter_ctrl(cmd):
    """OPEN or CLOSE green shutter"""
    channel = ':SYS-MOTION_'+ARM+'_SHUTTER_A_'+cmd
    ezca[channel] = 1

def pdh_ctrl(cmd):
    """ON or OFF for PDH servo"""
    if cmd == 'ON':
        v = 1
    elif cmd == 'OFF':
        v = 0
    else:
        raise ValueError()
    ezca['REFL_SERVO_IN1EN'] = v
    time.sleep(0.6)
    ezca['REFL_SERVO_COMCOMP'] = v
    time.sleep(0.6)
    ezca['REFL_SERVO_COMBOOST'] = v

def QPDs_bright():
    sum_threshold = 100
    #chans = map(lambda x: 'ALS-'+ARM+'_QPD_{var}_SUM_OUTPUT'.format(var=x),['A','B'])
    #time.sleep(1)
    #qpd_sums = cdsutils.avg(-1,chans,stddev=False)
    #return all(i > sum_threshold for i in qpd_sums)
    qpd_sums = []
    for qpd in ['A','B']:
        qpd_sums.append(ezca['QPD_'+qpd+'_NSUM_OUTPUT'])
    return all(i > sum_threshold for i in qpd_sums)


#def QPDs_centered():
#    r_max = 0.7
#    chans = map(lambda x: 'ALS-'+ARM+'_QPD_{var}_OUTPUT'.format(var=x),['A_PIT','B_PIT','A_YAW','B_YAW'])
#    time.sleep(1)
#    qpd_vals = cdsutils.avg(-1,chans,stddev=False)
#    return all(i < r_max for i in qpd_vals)

#def TransPD_bright():
#    channel = 'ALS-C_TR'+ARM+'_A_LF_OUTPUT'
#    transpd_val = cdsutils.avg(-3,channel,stddev=False)
#    return transpd_val > alsArmconfig.transpd_threshold[SYSTEM]

def Beatnote_good():
    beatampl_chan = 'FIBR_A_DEMOD_RFMON'
    beatampl_val = ezca[beatampl_chan]
    #log(beatampl_val)
    return beatampl_val>-28     #dBm

def BeatnoteFreq_good():
    beatfreq_chan = 'FIBR_LOCK_BEAT_FREQUENCY'
    beatfreq_val = ezca[beatfreq_chan]
    return abs(beatfreq_val-abs(alsArmconfig.beatnote_set[SYSTEM]))<5.0

def PLL_locked():
    return (ezca['FIBR_LOCK_TEMPERATURECONTROLS_ON'] and ezca['FIBR_SERVO_IN1EN'])

#def PZT_output_off():
#     for pzt in ['1','2']:
#         for dof in ['PIT','YAW']:
#             if ezca["PZT{}_{}_GAIN".format(pzt,dof)]==0:
#                 return True
#     return
    

##################################################
# Decorators
##################################################
class assert_qpds_bright(GuardStateDecorator):
    def pre_exec(self):
        if not QPDs_bright():
            log('no light on QPDs')
            return 'RESET_QPD_SERVOS'

#class assert_trans_bright(GuardStateDecorator):
#    def pre_exec(self):
#        if not TransPD_bright():
#            log('Light level low on Trans PD')
#            return ''

class assert_pll_locked(GuardStateDecorator):
    def pre_exec(self):
        if not (ezca['FIBR_LOCK_LOGIC_ENABLE'] == 0):
            ezca['FIBR_LOCK_LOGIC_ENABLE'] = 0
        if not Beatnote_good():
            log('Fiber PLL beat too far away, beginning Coarse Scan')
            return 'COARSE_SCAN_PLL'
        elif not BeatnoteFreq_good():
            log('Fiber PLL close but still too far away, beginning Fine Scan')
            return 'FINE_SCAN_PLL'
        elif not PLL_locked():
            log('Fiber PLL servo is off, turning on')
            return 'LOCK_PLL'

##################################################
##################################################
class INIT(GuardState):
    request = True



##################################################
class UNLOCK_PDH(GuardState):
    index = 80
    request = False

    def main(self):
        pdh_ctrl('OFF')
        #Change offsets for HWS measurement - ajm and cdb 171107
        #ezca['IP_POS_PIT_OFFSET'] = alsArmconfig.pos_pitoffset4tcs[SYSTEM]
        #ezca['IP_ANG_PIT_OFFSET'] = alsArmconfig.ang_pitoffset4tcs[SYSTEM]
        #ezca['IP_POS_YAW_OFFSET'] = alsArmconfig.pos_yawoffset4tcs[SYSTEM]
        #ezca['IP_ANG_YAW_OFFSET'] = alsArmconfig.ang_yawoffset4tcs[SYSTEM]

        # Moved the ISC_lock guardian CDB 2017 Nov 10
        #ezca['SUS-TMSY_M1_OPTICALIGN_Y_TRAMP'] = 10
        #time.sleep(0.5)
        #ezca['SUS-TMSY_M1_OPTICALIGN_Y_OFFSET'] += 4

        return True

##################################################
class UNLOCK_PDH_GOTO(GuardState):
    index = 81
    request = False
    goto = True

    def main(self):
        pdh_ctrl('OFF')
        #Change offsets for HWS measurement - ajm and cdb 171107
        #ezca['IP_POS_PIT_OFFSET'] = alsArmconfig.pos_pitoffset4tcs[SYSTEM]
        #ezca['IP_ANG_PIT_OFFSET'] = alsArmconfig.ang_pitoffset4tcs[SYSTEM]
        #ezca['IP_POS_YAW_OFFSET'] = alsArmconfig.pos_yawoffset4tcs[SYSTEM]
        #ezca['IP_ANG_YAW_OFFSET'] = alsArmconfig.ang_yawoffset4tcs[SYSTEM]

        # Moved the ISC_lock guardian CDB 2017 Nov 10
        #ezca['SUS-TMSY_M1_OPTICALIGN_Y_TRAMP'] = 10
        #time.sleep(0.5)
        #ezca['SUS-TMSY_M1_OPTICALIGN_Y_OFFSET'] += 4

        return True


##################################################
class TURN_OFF_QPD_SERVOS(GuardState):
    request = False
    index = 90

    def main(self):

        # Set TRAMPS
        ezca['IP_POS_PIT_TRAMP'] = 3
        ezca['IP_ANG_PIT_TRAMP'] = 3
        ezca['IP_POS_YAW_TRAMP'] = 3
        ezca['IP_ANG_YAW_TRAMP'] = 3
        time.sleep(0.3)

        # Turn off offsets
        ezca.switch('IP_POS_PIT', 'OFFSET', 'OFF')
        ezca.switch('IP_ANG_PIT', 'OFFSET', 'OFF')
        ezca.switch('IP_POS_YAW', 'OFFSET', 'OFF')
        ezca.switch('IP_ANG_YAW', 'OFFSET', 'OFF')
        time.sleep(3.5)

        # Turn off inputs
        ezca.switch('IP_POS_PIT', 'INPUT', 'OFF')
        ezca.switch('IP_ANG_PIT', 'INPUT', 'OFF')
        ezca.switch('IP_POS_YAW', 'INPUT', 'OFF')
        ezca.switch('IP_ANG_YAW', 'INPUT', 'OFF')

        return True

##################################################
class SHUTTER_GREEN(GuardState):
    index = 95
    request = False

    def main(self):
        shutter_ctrl('CLOSE')
        #test als misalignemt for HWS 
        # Turn off PDH
        pdh_ctrl('OFF')
        time.sleep(1)
        # Set ramp times
        for dof1 in ['POS', 'ANG']:
            for dof2 in ['PIT', 'YAW']:
                ezca['IP_' + dof1 + '_' + dof2 + '_TRAMP'] = 5.0

        #ezca.switch('IP_POS_YAW', 'OFFSET', 'ON')
        #ezca.switch('IP_ANG_YAW', 'OFFSET', 'ON')

        #ezca['IP_POS_YAW_OFFSET'] = alsArmconfig.pos_yawoffset4tcs[SYSTEM]
        #ezca['IP_ANG_YAW_OFFSET'] = alsArmconfig.ang_yawoffset4tcs[SYSTEM]

        time.sleep(5)
    	# TODO:
        # Make a new state for ETM HWS alignment 
        # Then in shutter turn off QPD servos and PDH Loop

        return True

##################################################
class SHUTTERED(GuardState):
    index = 100

    def run(self):
        return True

##################################################
class PZT_MIRRORS_OFF(GuardState):
    index = 110
    request = True

    def main(self):

        tramp = 10

        for pzt in ['1','2']:
            for dof in ['PIT','YAW']:
                ezca['PZT'+pzt+'_'+dof+'_TRAMP'] = tramp
        time.sleep(0.5)

        for pzt in ['1','2']:
            for dof in ['PIT','YAW']:
                ezca['PZT'+pzt+'_'+dof+'_GAIN'] = 0

        self.timer['pztsoff'] = tramp

    def run(self):

        if not self.timer['pztsoff']:
            return
        return True

##################################################
class PZT_MIRRORS_ON(GuardState):
    index = 120
    request = False

    def main(self):

        tramp = 10

        for pzt in ['1','2']:
            for dof in ['PIT','YAW']:
                ezca['PZT'+pzt+'_'+dof+'_TRAMP'] = tramp
        time.sleep(0.5)

        for pzt in ['1','2']:
            for dof in ['PIT','YAW']:
                ezca['PZT'+pzt+'_'+dof+'_GAIN'] = 1

        self.timer['pztson'] = tramp

    def run(self):

        if not self.timer['pztson']:
            return
        return True


##################################################
class RELEASE_GREEN_PHOTONS(GuardState):
    index = 10
    request = False

    def main(self):
        shutter_ctrl('OPEN')
        self.timer['photons'] = 5

    def run(self):
        return self.timer['photons']


##################################################
class RESET_QPD_SERVOS(GuardState):
    index = 20
    request = False

    def main(self):

        # Reset PZT offsets for als alignemnt
        #ezca['IP_POS_YAW_OFFSET'] = alsArmconfig.pos_yawoffset4servo[SYSTEM]
        #ezca['IP_ANG_YAW_OFFSET'] = alsArmconfig.ang_yawoffset4servo[SYSTEM]
        #ezca['IP_POS_PIT_OFFSET'] = alsArmconfig.pos_pitoffset4servo[SYSTEM]
        #ezca['IP_ANG_PIT_OFFSET'] = alsArmconfig.ang_pitoffset4servo[SYSTEM]

        # Turn off pos yaw offset
        ezca['IP_POS_PIT_TRAMP'] = 0
        ezca['IP_ANG_PIT_TRAMP'] = 0
        ezca['IP_POS_YAW_TRAMP'] = 0
        ezca['IP_ANG_YAW_TRAMP'] = 0

        time.sleep(0.3)

        ezca.switch('IP_POS_PIT', 'OFFSET', 'OFF')
        ezca.switch('IP_ANG_PIT', 'OFFSET', 'OFF')
        ezca.switch('IP_POS_YAW', 'OFFSET', 'OFF')
        ezca.switch('IP_ANG_YAW', 'OFFSET', 'OFF')

        # Turn off inputs
        ezca.switch('IP_POS_PIT', 'INPUT', 'OFF')
        ezca.switch('IP_ANG_PIT', 'INPUT', 'OFF')
        ezca.switch('IP_POS_YAW', 'INPUT', 'OFF')
        ezca.switch('IP_ANG_YAW', 'INPUT', 'OFF')

        # Turn off 20dB
        ezca.switch('IP_POS_PIT', 'FM6', 'OFF')
        ezca.switch('IP_ANG_PIT', 'FM6', 'OFF')
        ezca.switch('IP_POS_YAW', 'FM6', 'OFF')
        ezca.switch('IP_ANG_YAW', 'FM6', 'OFF')

        time.sleep(0.3)

        # Clear histories
        ezca['IP_POS_PIT_RSET'] = 2
        ezca['IP_ANG_PIT_RSET'] = 2
        ezca['IP_POS_YAW_RSET'] = 2
        ezca['IP_ANG_YAW_RSET'] = 2

        self.timer['wait'] = 5
        chans = ['QPD_{}_OUTPUT'.format(x) for x in ['A_PIT','B_PIT','A_YAW','B_YAW']]
        self.avg = runningAvg.RunningAvg(ezca, chans, 3)


    def run(self):
        if not QPDs_bright():
            notify('No light on at least one QPD')
            return
        # Check if spots are centered well enough on QPDs
        qpd_vals, done = self.avg.avg()
        #log(str(qpd_vals))
        r_max = 0.7
        if not done:
            return
        if not all(abs(i) < r_max for i in qpd_vals):
            notify('Spots on QPDs need to be centered better')
            return
        #if not QPDs_centered():
        #    notify('Spots on QPDs need to be centered better')
        #    return  
        return self.timer['wait']

##################################################
class CHECK_QPD_SUM(GuardState):
    index = 30
    request = False

    def main(self):
        #power_qpda = cdsutils.avg(-3,'QPD_A_SUM_OUTPUT',stddev=False)
        #power_qpdb = cdsutils.avg(-3,'QPD_B_SUM_OUTPUT',stddev=False)
        power_qpda = ezca['QPD_A_SUM_OUTPUT']
        power_qpdb = ezca['QPD_B_SUM_OUTPUT']
        if power_qpda < 100 or power_qpdb < 100:
            return 'QPDS_DARK'
        return True

#################################################
#TODO: Make this state check qpd sums and that the beams are approximately centered
class QPDS_DARK(GuardState):
    index = 35
    request = False

    #def main(self):
        

    def run(self):

        #if QPDs_bright():
        #    if QPDs_centered():
        #        

        #if not QPDs_bright():
        #    notify('No light on QPD')
        #    return
        #
        #if not QPDs_centered():
        #    notify('QPDs need centering')
        #    return
        #
        #return LOCK_QPD_SERVOS

        notify('No light on QPD')
        return True

##################################################
class LOCK_QPD_SERVOS(GuardState):
    index = 40
    request = False

    @assert_qpds_bright
    def main(self):

        #TODO: make PREP_QPD_LOCKING state which sets gains, filters and other.

        # Turn on outputs
        ezca.switch('IP_POS_PIT', 'OUTPUT', 'ON')
        ezca.switch('IP_ANG_PIT', 'OUTPUT', 'ON')
        ezca.switch('IP_POS_YAW', 'OUTPUT', 'ON')
        ezca.switch('IP_ANG_YAW', 'OUTPUT', 'ON')

        # Turn on inputs
        ezca.switch('IP_POS_PIT', 'INPUT', 'ON')
        ezca.switch('IP_ANG_PIT', 'INPUT', 'ON')
        ezca.switch('IP_POS_YAW', 'INPUT', 'ON')
        ezca.switch('IP_ANG_YAW', 'INPUT', 'ON')

        self.timer['wait'] = 2

    @assert_qpds_bright
    def run(self):
        if not self.timer['wait']:
            return
        
        # Turn on 20dB
        # JCB 2015/05/27 Removed 20dB because made loops unstable at Y end
        #ezca.switch('IP_POS_PIT', 'FM6', 'ON')
        #ezca.switch('IP_ANG_PIT', 'FM6', 'ON')
        #ezca.switch('IP_POS_YAW', 'FM6', 'ON')
        #ezca.switch('IP_ANG_YAW', 'FM6', 'ON')

        return True

##################################################
class OFFLOAD_QPD_SERVOS(GuardState):
    index = 45
    request = False

    @assert_qpds_bright
    def main(self):

        # Measure control signals
        #offset_pzt1_pit = cdsutils.avg(-3,'PZT1_PIT_OUTPUT',stddev=False)
        #offset_pzt2_pit = cdsutils.avg(-3,'PZT2_PIT_OUTPUT',stddev=False)
        #offset_pzt1_yaw = cdsutils.avg(-3,'PZT1_YAW_OUTPUT',stddev=False)
        #offset_pzt2_yaw = cdsutils.avg(-3,'PZT2_YAW_OUTPUT',stddev=False)
        offset_pzt1_pit = ezca['PZT1_PIT_OUTPUT']
        offset_pzt2_pit = ezca['PZT2_PIT_OUTPUT']
        offset_pzt1_yaw = ezca['PZT1_YAW_OUTPUT']
        offset_pzt2_yaw = ezca['PZT2_YAW_OUTPUT']

        # Ramp times
        for pzt in ['PZT1','PZT2']:
            for dof in ['PIT','YAW']:
                ezca[pzt + '_' + dof + '_TRAMP'] = 3
        time.sleep(0.3)

        # Offload
        ezca['PZT1_PIT_OFFSET'] = offset_pzt1_pit
        ezca['PZT2_PIT_OFFSET'] = offset_pzt2_pit
        ezca['PZT1_YAW_OFFSET'] = offset_pzt1_yaw
        ezca['PZT2_YAW_OFFSET'] = offset_pzt2_yaw

    @assert_qpds_bright
    def run(self):
        return True

##################################################
class OFFSET_QPD_SERVOS(GuardState):
    index = 47
    request = False

    @assert_qpds_bright
    def main(self):

        #Reset ALS offsets
        ezca['IP_POS_YAW_OFFSET'] = alsArmconfig.pos_yawoffset4servo[SYSTEM]
        ezca['IP_ANG_YAW_OFFSET'] = alsArmconfig.ang_yawoffset4servo[SYSTEM]
        ezca['IP_POS_PIT_OFFSET'] = alsArmconfig.pos_pitoffset4servo[SYSTEM]
        ezca['IP_ANG_PIT_OFFSET'] = alsArmconfig.ang_pitoffset4servo[SYSTEM]

        # Turn on pos yaw offset
        for dof in ['PIT','YAW']:
            ezca['IP_POS_' + dof + '_TRAMP'] = 3
            ezca['IP_ANG_' + dof + '_TRAMP'] = 3
            time.sleep(0.3)
            ezca.switch('IP_POS_' + dof, 'OFFSET', 'ON')
            ezca.switch('IP_ANG_' + dof, 'OFFSET', 'ON')
        #ezca['']    

    @assert_qpds_bright
    def run(self):
        return True

##################################################
class QPDS_LOCKED(GuardState):
    index = 50

    @assert_qpds_bright
    def run(self):
        return True

##################################################
class PREP_PDH_LOCK(GuardState):
    index = 54
    request = False

    @assert_qpds_bright
    @assert_pll_locked     # AJM20191028 commented out because no comparator
    def main(self):
        ezca['REFL_SERVO_IN1GAIN'] = alsArmconfig.pdh_acq_gain[SYSTEM]
        pdh_ctrl('OFF')
        self.timer['unlock'] = 2

    @assert_qpds_bright
    @assert_pll_locked
    def run(self):
        if not self.timer['unlock']:
            return
        return True

##################################################
class RELOCK_PDH(GuardState):
    index = 55
    request = False

    @assert_qpds_bright
    def main(self):
        #ezca['REFL_SERVO_IN1GAIN'] = alsArmconfig.pdh_acq_gain[SYSTEM]  #remove for state split AJM20190628
        pdh_ctrl('ON')
        self.timer['pdhlock'] = 3

    @assert_qpds_bright
    def run(self):
        #pdh_ctrl('OFF')
        #time.sleep(2)
        #pdh_ctrl('ON')
        #time.sleep(2)   #was 6

        if not self.timer['pdhlock']:
            return

        if abs(ezca['REFL_SERVO_FASTMON']) > 1.5:
            #return False
            return 'PREP_PDH_LOCK'        
        if ezca[':ALS-C_TR'+ARM+'_A_LF_OUTPUT'] < alsArmconfig.transpd_threshold[SYSTEM]: #AP 0926, commented out because of the epics freeze issue 
            #return False
            return 'PREP_PDH_LOCK'
        return True


##################################################
class BOOST_PDH_GAIN(GuardState):
    index = 57
    request = False

    @assert_qpds_bright
    def main(self):
        acqgain = ezca['REFL_SERVO_IN1GAIN']
        #time.sleep(1)
        #nStep =  alsArmconfig.pdh_lock_gain[SYSTEM] - alsArmconfig.pdh_acq_gain[SYSTEM]
        nStep = alsArmconfig.pdh_lock_gain[SYSTEM] - acqgain
        cmd_string = str(1) + ',' + str(nStep)
        self.step = cdsutils.Step(ezca,'REFL_SERVO_IN1GAIN',cmd_string,time_step=0.2)

    @assert_qpds_bright
    def run(self):
        return self.step.step()


##################################################
class PDH_LOCKED(GuardState):
    index = 60
    request = True

    #JCB
    @assert_qpds_bright
    def main(self):
        channel = ':ALS-C_TR'+ARM+'_A_LF_OUTPUT'
        #if ARM == 'X':
        #    BEAT = 'COMM'
        #else:
        #    BEAT = 'DIFF'
        #channel = ':ALS-C_'+BEAT+'_A_DEMOD_RFMON'
        self.avg = runningAvg.RunningAvg(ezca, channel, 3)


    @assert_qpds_bright
    def run(self):
        #JCB, breaking prefix
        transpd_val, done = self.avg.avg()
        if done:
            #if ARM == 'X':
            #    SYSTEM2 = 'COMM'
            #else:
            #    SYSTEM2 = 'DIFF'
            if transpd_val > alsArmconfig.transpd_threshold[SYSTEM]: # TH 180926 changed to monitor diff
            #if transpd_val > alsArmconfig.transpd_threshold[SYSTEM2]:
                log('PDH is locked')
                return True
            else:
                return 'PREP_PDH_LOCK'
        else:
            return False

        #if trans power is below some threshold, jump back to RELOCK_PDH
        #time.sleep(5)
        #if not TransPD_bright():
        #    return 'RELOCK_PDH'
        #return True



##################################################
class COARSE_SCAN_PLL(GuardState):
    index = 51
    request = False

    def main(self):

        #Ensure Beckhoff PLL Autolcker is Disabled
        ezca['FIBR_LOCK_LOGIC_ENABLE'] = 0

        #Turn off temperature feedback
        ezca['FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 0


        beatAmplChan = 'FIBR_A_DEMOD_RFMON'
        beatFreqChan = 'FIBR_LOCK_BEAT_FREQUENCY'
        self.crystalFreqChan = 'LASER_HEAD_CRYSTALFREQUENCY'

        fstart = -700
        fstop = 700
        nsteps = 8

        # Will carry out a coarse scan from -700 to +700 of 8 steps
        self.laserScan = LaserFreqScan(ezca,fstart,fstop,nsteps,self.crystalFreqChan,beatAmplChan,beatFreqChan,3)

        #self.do_once = True

    def run(self):

        [done,crystalFreqCoarse,beatAmplCoarse,beatFreqCoarse] = self.laserScan.scan()

        if not done:
            return

        #if self.do_once:
        #    self.do_once = False

        index_beatmax = np.argmax(beatAmplCoarse)
        crystalFreq_beatmax = crystalFreqCoarse[index_beatmax]
        # Place the crystal frequency at the place where the beat amplitude is maximum. 
        ezca[self.crystalFreqChan] = crystalFreq_beatmax
        return True


##################################################
class FINE_SCAN_PLL(GuardState):
    index = 52
    request = False

    def main(self):

        #Ensure Beckhoff PLL Autolcker is Disabled
        ezca['FIBR_LOCK_LOGIC_ENABLE'] = 0

        #Turn off temperature feedback
        ezca['FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 0

        beatAmplChan = 'FIBR_A_DEMOD_RFMON'
        beatFreqChan = 'FIBR_LOCK_BEAT_FREQUENCY'
        self.crystalFreqChan = 'LASER_HEAD_CRYSTALFREQUENCY'

        #fstart = crystalFreq_beatmax-100
        #fstop = crystalFreq_beatmax+100
        fstart = ezca[self.crystalFreqChan]-100
        fstop = ezca[self.crystalFreqChan]+100
        nsteps = 6

        # Will carry out a fine scan from -100 to +100 of 6 steps
        self.laserScan = LaserFreqScan(ezca,fstart,fstop,nsteps,self.crystalFreqChan,beatAmplChan,beatFreqChan,5)

        #self.do_once = True

    def run(self):

        [done,crystalFreqFine,beatAmplFine,beatFreqFine] = self.laserScan.scan()
        if not done:
            return

        #if self.do_once:
        #    self.do_once = False

        # Give the beat frequency (which is the absolute of the frequency difference) a sign from the beat amplitude slope
        beatAmplSlope = np.gradient(beatAmplFine)
        beatSignedFreq = np.multiply(beatFreqFine, np.sign(beatAmplSlope))

        # Fit with a simple poly1d fit (i.e. a line), then find crystal frequency that gives the correct beatnote frequency
        fit_coeff = np.polyfit(beatSignedFreq,crystalFreqFine,1)
        p = np.poly1d(fit_coeff)
        crystalFreqFinal = p(alsArmconfig.beatnote_set[SYSTEM])
        ezca[self.crystalFreqChan] = crystalFreqFinal
        return True

##################################################
class LOCK_PLL(GuardState):
    index = 53
    request = False

    def main(self):

        # Ensure PLL servo is on
        ezca['FIBR_SERVO_IN1EN'] = 1
        self.timer['pll'] = 5

        self.do_once = True

    def run(self):

        if not self.timer['pll']:
            return

        if self.do_once:
            self.do_once = False
            ezca['FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 1
            self.timer['pll'] = 15
            return
    
        return True


##################################################
class ALIGN_ARM(GuardState):
    index = 70
    request = False

    #JCB
    @assert_qpds_bright
    def main(self):

        return True
        #pit_readback = ':CAM-ITM'+ARM+'_Y'
        #yaw_readback = ':CAM-ITM'+ARM+'_X'

        #TODO: make sure test banks have correct calibration and offsets are on

        #pit_control = ':SUS-ETM'+ARM+'_M0_TEST_P_OFFSET'
        #yaw_control = ':SUS-ETM'+ARM+'_M0_TEST_Y_OFFSET'

        #trigger = ':ALS-C_TR'+ARM+'_A_LF_OUTPUT'

        #self.servo = cdsutils.trigservo(ezca, pit_control, pit_readback, gain=1.0, triggerChannel=trigger, triggerOn=alsArmconfig.transpd_threshold[SYSTEM], triggerOff=alsArmconfig.transpd_threshold[SYSTEM]*0.9)


    @assert_qpds_bright
    def run(self):

        return True


##################################################
##################################################
edges = [
    ('INIT', 'RELEASE_GREEN_PHOTONS'),
    ('INIT', 'TURN_OFF_QPD_SERVOS'),

    ('UNLOCK_PDH', 'TURN_OFF_QPD_SERVOS'),
    ('TURN_OFF_QPD_SERVOS', 'SHUTTER_GREEN'),
    ('SHUTTER_GREEN', 'SHUTTERED'),
    ('SHUTTERED','PZT_MIRRORS_OFF'),
    ('PZT_MIRRORS_OFF','PZT_MIRRORS_ON'),
    ('PZT_MIRRORS_ON','SHUTTERED'),
    ('SHUTTERED', 'RELEASE_GREEN_PHOTONS'), 
    ('RELEASE_GREEN_PHOTONS', 'RESET_QPD_SERVOS'),
    ('RESET_QPD_SERVOS', 'LOCK_QPD_SERVOS'),
    #('RESET_QPD_SERVOS', 'CHECK_QPD_SUM'),
    #('CHECK_QPD_SUM', 'LOCK_QPD_SERVOS'),
    #('CHECK_QPD_SUM', 'QPDS_DARK'),
    ('LOCK_QPD_SERVOS', 'OFFLOAD_QPD_SERVOS'),
    ('OFFLOAD_QPD_SERVOS', 'OFFSET_QPD_SERVOS'),
    ('OFFSET_QPD_SERVOS','QPDS_LOCKED'),
    #('QPDS_LOCKED', 'RELOCK_PDH'),
    ('QPDS_LOCKED', 'PREP_PDH_LOCK'),
    ('PREP_PDH_LOCK', 'RELOCK_PDH'),
    ('RELOCK_PDH', 'BOOST_PDH_GAIN'),
    ('BOOST_PDH_GAIN', 'PDH_LOCKED'),
    #('PDH_LOCKED','QPDS_LOCKED'),   # AJM 190618 should have it go through UNLOCK_PDH first
    ('PDH_LOCKED','PREP_PDH_LOCK'),
    ('PREP_PDH_LOCK','QPDS_LOCKED'),
    ('PDH_LOCKED', 'UNLOCK_PDH'),
    ('UNLOCK_PDH_GOTO','TURN_OFF_QPD_SERVOS'),
    #('PDH_LOCKED', 'RELOCK_PDH'),
    #('RELOCK_PDH', 'UNLOCK_PDH'), # TH 9/14/18 added for RH tests, to avoid stuck in PDH loop when trying to shutter (won't work AJM)
    ('QPDS_LOCKED', 'UNLOCK_PDH'), # TH same as above
    ('COARSE_SCAN_PLL','FINE_SCAN_PLL'),
    ('FINE_SCAN_PLL','LOCK_PLL'),
    #('LOCK_PLL','RELOCK_PDH'),
    ('LOCK_PLL','PREP_PDH_LOCK'),
    ]
